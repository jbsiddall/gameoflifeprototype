
from tkinter import *
from random import random
from threading import Thread
from time import sleep
from sys import argv
from math import floor

class Window:

	def __init__(self, master,board, boardWidth, boardHeight, width, height):
		self.board = board
		self.ids = {}
		frame = Frame(master, width=width,height=height)
		self.frame = frame
		frame.pack(pady=10)

		interface = Frame(frame)
		self.interface = interface
		interface.pack()
			
		self.button = Button(interface, text="next")
		self.button.pack(side=LEFT)

		self.auto = Button(interface, text="auto")
		self.auto.pack(side=LEFT)

		self.stop = Button(interface, text="stop")
		self.stop.pack(side=LEFT)

		self.canvas = Canvas(frame,width=width,height=height)
		
		self.canvas.pack()

		self.canvas.bind("<Button-1>",self.clicked)

		self.boardWidth = boardWidth
		self.boardHeight = boardHeight

		self.canvasWidth = width 
		self.canvasHeight = height 

		self.squareWidth = self.canvasWidth / self.boardWidth
		self.squareHeight = self.canvasHeight / self.boardHeight

		self.locations = [(x,y) for x in range(self.boardWidth) for y in range(self.boardHeight) ]
		
		#self.drawBorder()
		#self.drawBoard()

	def clicked(self, event):

		x = floor(event.x / self.squareWidth)
		y = floor(event.y / self.squareHeight)

		if (0 <= x < self.board.width) and (0 <= y < self.board.height):
			cell = self.board.cells[(x,y)]
			if cell.status == Cell.STATUS_ALIVE:
				name = "alive"
			elif cell.status == Cell.STATUS_DEAD:
				name = "dead"
			else:
				raise exception("unkown status")
			print(cell)
		

	def drawBorder(self):
		topLeft = self.getActualPoint((0,0))
		bottomRight = self.getActualPoint((self.boardWidth,self.boardHeight))

		self.canvas.create_rectangle(topLeft[0],topLeft[1],bottomRight[0],bottomRight[1])

	def drawBoard(self):
		
		size = (self.squareWidth, self.squareHeight)

		for (x, y) in self.locations:
				acoord = self.getActualPoint((x,y))
				tx = acoord[0]
				ty = acoord[1]

				bx = tx + size[0]
				by = ty + size[1]

				self.canvas.create_rectangle(tx, ty, bx, by)

	
	def drawCell(self, x, y, status):
		if status:
			if (x, y) not in self.ids:
				size = (self.squareWidth, self.squareHeight)
				acoord = self.getActualPoint((x,y))
				tx = acoord[0]
				ty = acoord[1]

				bx = tx + size[0]
				by = ty + size[1]

				self.ids[(x,y)] = (tx, ty, bx, by, 0, self.canvas.create_rectangle(tx, ty, bx, by, fill='black'))
			else:
				rec = self.ids[(x,y)]
				tx = rec[0]
				ty = rec[1]
				bx = rec[2]
				by = rec[3]
				age = rec[4]
				item = rec[5]
				
				if age == 0:
					self.canvas.delete(item)
					del self.ids[(x,y)]
					self.ids[(x,y)] = (tx, ty, bx, by, 1, self.canvas.create_rectangle(tx, ty, bx, by, fill='black'))

		else:
			if (x, y) in self.ids:
				self.canvas.delete(self.ids[(x,y)][5])
				del self.ids[(x,y)]


	def getActualPoint(self, loc):

		x, y = loc

		wx = x
		wy = self.boardHeight - 1 - y

		vx = wx * self.squareWidth
		vy = wy * self.squareHeight

		return (vx+1, vy+1)

	
	def update(self, boardStatus):

		for (x,y) in self.locations:
				self.drawCell(x, y, boardStatus[x][y])






class Cell:
	
	STATUS_DEAD = 0
	STATUS_ALIVE = 1

	def __init__(self, status, x, y):
		if status not in [self.STATUS_DEAD, self.STATUS_ALIVE]:
			raise Exception("unkown status " + status)

		self.status = status
		self.x = x
		self.y = y

	def update(self, boardStatus):
		neighbours = self.neighbours(boardStatus)
		alive = self.status == Cell.STATUS_ALIVE

		if alive:
			if neighbours < 2: self.status = Cell.STATUS_DEAD
			elif neighbours in [2,3]: self.status = Cell.STATUS_ALIVE
			elif neighbours > 3: self.status = Cell.STATUS_DEAD
		else:
			if neighbours == 3: self.status = Cell.STATUS_ALIVE

	def neighbours(self, boardStatus):
		locations = [(x,y) for x in range(self.x-1, self.x+2) for y in range(self.y-1, self.y+2)]

		num = 0

		for (x,y) in [(x, y) for (x,y) in locations if x >= 0 and y >= 0 and y < len(boardStatus) and x < len(boardStatus[0])]:
			if boardStatus[x][y] and (self.x,self.y) != (x,y):
				num += 1

		return num

	def __repr__(self):
		return "Cell({},{},{})f".format(self.status, self.x, self.y)
		


	
	
		

class Board:

	ALIVE_RATE = 0.07 

	status = [
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 1, 1, 0],
		[0, 0, 1, 1, 0],
		[0, 0, 0, 0, 0]
	]

	def __init__(self, width, height):
		self.width = width
		self.height = height

		self.cells = dict( ((x,y), Cell(self.cellStatus(x,y), x, y)) for x in range(width) for y in range(height) )


	def update(self):
		status = self.getStatus()
		for cell in self.cells.values():
				cell.update(status)

	
	def cellStatus(self, x, y):
		#return Board.status[self.height-y-1][x]
		return random() < Board.ALIVE_RATE


	def getStatus(self):
		board = [[False for y in range(self.height)] for x in range(self.width)]
		for ((x, y), cell) in self.cells.items():  board[x][y] = (cell.status == Cell.STATUS_ALIVE)
		return board


	def numberCells(self):
		count = 0
		for x in range(self.width):
			for y in range(self.height):
				if self.cells[(x,y)].status == Cell.STATUS_ALIVE:
					count += 1
		return count


	def __repr__(self):
		boardStatus = self.getStatus()
		string = ""
		for y in range(self.height-1, -1, -1):
			for x in range(self.width):
				string += repr(self.cells[(x,y)].neighbours(boardStatus)) + " "
			string += "\n"
		return string










class Runner:

	def __init__(self, board, window):
		self.board = board
		self.window = window
		self.window.update(self.board.getStatus())

	def run(self):
			self.board.update()
			self.window.update(self.board.getStatus())

		

args = {"width":100, "height":100, "ups":5}
for arg in argv[1:]:
	key, value = arg.split(":")
	args[key] = int(value)


root = Tk()
root.resizable(0,0)
root.title("Siddall's Game of Life")
windowSize = (800, 600)
size = (args["width"], args["height"])
delay= int(1000 / args["ups"])
board = Board(size[0], size[1])
window = Window(root, board, size[0], size[1], windowSize[0], windowSize[1])
runner = Runner(board, window)

doing = False
def do(event):
	global doing
	if not doing:
		doing = True
		runner.run()
		doing = False

timer  = False
timerRunning = False
def startTimer(event):
	global timer
	global timerRunning
	if event != None:
		if not timer:
			timer = True
			timerRunning = True
			root.after(delay, startTimer, None)
	else:
		if timerRunning:	
			do(None)
			root.after(delay, startTimer, None)


def stopTimer(event):
	global timer
	global timerRunning
	timer = False
	timerRunning = False

window.button.bind("<Button-1>", do)
window.auto.bind("<Button-1>", startTimer)
window.stop.bind("<Button-1>",stopTimer)
root.mainloop()
print("finished")




